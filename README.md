Interview Question for Hamiltonian Dynamics

# Requirements 

* Maven [Installation Instructions](https://maven.apache.org/install.html)
* JDK 15+ [Installation Instructions](https://openjdk.java.net/install/)

# Installation

Navigate to root directory and run
```
mvn clean install
```

# To Run

At the root directory:

Run ```mvn package``` to package the Jar. 

Run ```java -jar target/opennlp-1.0-SNAPSHOT.jar``` to execute the program.

Alternatively, if using an IDE such as IntelliJ / Eclipse, you can run the Main class.

# The Task

The included code will provide you with a pre-trained model for extracting name entities (e.g. Names of Places, Buildings).
Your task includes the following.

1. Extract named entities from a string using the provided model. 