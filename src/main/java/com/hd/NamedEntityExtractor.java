package com.hd;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;

public class NamedEntityExtractor {

    private final NameFinderME finder;

    /**
     * Primary Constructor.
     *
     * @param model the named entity model.
     */
    public NamedEntityExtractor(final TokenNameFinderModel model) {
        this.finder = new NameFinderME(model);
    }


    public void printNamedEntities() {

        String sentence = "If President John F. Kennedy, after visiting France in 1961 with his immensely popular wife";

        // Split the sentence into tokens

        // Find the names in the tokens

        // Print the names extracted

    }

}
