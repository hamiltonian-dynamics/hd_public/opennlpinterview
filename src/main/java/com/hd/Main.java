package com.hd;

import opennlp.tools.namefind.TokenNameFinderModel;

import java.io.File;
import java.io.IOException;

public class Main {

    /**
     * Loads the en-ner-person.bin file from the resources folder.
     *
     * DO NOT CHANGE.
     *
     * @return the token name finder model
     * @throws IOException when the model has not been found.
     */
    public static TokenNameFinderModel loadModel() throws IOException {
        return new TokenNameFinderModel(new File("input/en-ner-person.bin"));
    }

    public static void main(String[] args) throws IOException {

        final TokenNameFinderModel tokenNameFinderModel = loadModel();

        NamedEntityExtractor namedEntityExtractor = new NamedEntityExtractor(tokenNameFinderModel);

        namedEntityExtractor.printNamedEntities();

    }
}
